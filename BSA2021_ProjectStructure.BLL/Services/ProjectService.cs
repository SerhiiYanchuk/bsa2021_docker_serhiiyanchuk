﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async System.Threading.Tasks.Task<IEnumerable<ProjectDTO>> GetAll()
        {
            var projects = await _unitOfWork.Projects.GetAll(false);
            foreach(var project in projects)
            {
                await _unitOfWork.Projects.LoadAuthor(project);
                await _unitOfWork.Projects.LoadTeam(project);
                await _unitOfWork.Projects.LoadTasks(project);
            }           
            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }
        public async System.Threading.Tasks.Task<ProjectDTO> FindById(int projectId)
        {
            var project = await _unitOfWork.Projects.FindById(projectId);
            await _unitOfWork.Projects.LoadAuthor(project);
            await _unitOfWork.Projects.LoadTeam(project);
            await _unitOfWork.Projects.LoadTasks(project);
            return _mapper.Map<ProjectDTO>(project);
        }
        public async System.Threading.Tasks.Task<ProjectDTO> Insert(NewProjectDTO projectDTO)
        {
            var createdProject = _mapper.Map<Project>(projectDTO);
            await _unitOfWork.Projects.Insert(createdProject);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<ProjectDTO>(createdProject);
        }

        public async System.Threading.Tasks.Task Update(ProjectDTO projectDTO)
        {
            Project editProject = await _unitOfWork.Projects.FindById(projectDTO.Id);
            _mapper.Map<ProjectDTO, Project>(projectDTO, editProject);
            _unitOfWork.Projects.Update(editProject);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int projectId)
        {
            await _unitOfWork.Projects.Delete(projectId);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(ProjectDTO projectDTO)
        {
            _unitOfWork.Projects.Delete(_mapper.Map<Project>(projectDTO));
            await _unitOfWork.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task<bool> CheckAvailability(int projectId)
        {
            return await _unitOfWork.Projects.CheckAvailability(projectId);
        }
       
    }
}
