﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.Common.DTO.QueryResultDTO;
using BSA2021_ProjectStructure.Common.EqualityComparer;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class LinqSelectionService : ILinqSelectionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public LinqSelectionService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        private async Task<IEnumerable<ProjectDTO>> CreateStructure()
        {
            IEnumerable<ProjectDTO> projects = _mapper.Map<IEnumerable<ProjectDTO>>(await _unitOfWork.Projects.GetAll(false));
            IEnumerable<TaskDTO> tasks = _mapper.Map<IEnumerable<TaskDTO>>(await _unitOfWork.Tasks.GetAll(false));
            IEnumerable<TeamDTO> teams = _mapper.Map<IEnumerable<TeamDTO>>(await _unitOfWork.Teams.GetAll(false));
            IEnumerable<UserDTO> users = _mapper.Map<IEnumerable<UserDTO>>(await _unitOfWork.Users.GetAll(false));

            var tasksWithPerformers = from task in tasks
                                      join performer in users on task.PerformerId equals performer.Id
                                      select new TaskDTO
                                      {
                                          Id = task.Id,
                                          ProjectId = task.ProjectId,
                                          PerformerId = task.PerformerId,
                                          Performer = performer,
                                          Name = task.Name,
                                          Description = task.Description,
                                          State = task.State,
                                          CreatedAt = task.CreatedAt,
                                          FinishedAt = task.FinishedAt,
                                      };

            return (from project in projects
                    join task in tasksWithPerformers on project.Id equals task.ProjectId into taskList
                    join author in users on project.AuthorId equals author.Id
                    join team in teams on project.TeamId equals team.Id
                    join member in users on team.Id equals member.TeamId into memberList
                    select new ProjectDTO
                    {
                        Id = project.Id,
                        AuthorId = project.AuthorId,
                        Author = author,
                        TeamId = project.TeamId,
                        Team = new TeamDTO
                        {
                            Id = team.Id,
                            Name = team.Name,
                            CreatedAt = team.CreatedAt,
                            Members = memberList.ToList()
                        },
                        Name = project.Name,
                        Description = project.Description,
                        Deadline = project.Deadline,
                        Tasks = taskList.ToList()
                    }).ToList();
        }
        public async Task<IDictionary<ProjectDTO, int>> GetUserProjectsWithQuantityTask(int performerId)
        {
            IEnumerable<ProjectDTO> projects = await CreateStructure();
            return projects.SelectMany(p => p.Tasks, (p, t) => new { Project = p, Task = t })
                            .Where(p => p.Task.PerformerId == performerId)
                            .GroupBy(p => p.Project, new ProjectEqualityComparer())
                            .ToDictionary(g => g.Key, g => g.Count());
        }

        public async Task<IEnumerable<TaskDTO>> GetUserTasks(int performerId)
        {
            IEnumerable<ProjectDTO> projects = await CreateStructure();
            const int MaxNameLength = 45;
            return projects.SelectMany(p => p.Tasks)
                           .Where(t => t.PerformerId == performerId && t.Name.Length < MaxNameLength)
                           .ToList();                        
        }

        public async Task<IEnumerable<FinishedTaskDTO>> GetFinishedTaskInCurrentYear(int performerId)
        {
            IEnumerable<ProjectDTO> projects = await CreateStructure();
            const int CurrentYear = 2021;
            return projects.SelectMany(p => p.Tasks)
                            .Where(t => t.PerformerId == performerId)
                            .Where(t => t.FinishedAt.HasValue && t.FinishedAt.Value.Year == CurrentYear)
                            .Select(t => new FinishedTaskDTO
                            {
                                Id = t.Id,
                                Name = t.Name
                            })
                            .ToList();
        }

        public async Task<IEnumerable<TeamShortInfoDTO>> GetTeamShortInfo()
        {
            IEnumerable<ProjectDTO> projects = await CreateStructure();
            const int MinAge = 10;
            return projects.Select(p => p.Team)
                           .Distinct(new TeamEqualityComparer())
                           .Where(t => t.Members.All(m => DateTime.Now.Year - m.Birthday.Year >= MinAge))
                           .Select(t => new TeamShortInfoDTO
                           {
                               Id = t.Id,
                               Name = t.Name,
                               Members = t.Members.OrderByDescending(m => m.RegisteredAt).ToList()
                           })
                          .ToList();
        }

        public async Task<IEnumerable<PerformerWithTasksDTO>> GetUsersWithTasks()
        {
            IEnumerable<ProjectDTO> projects = await CreateStructure();
            return projects.SelectMany(p => p.Tasks)
                           .GroupBy(t => t.Performer, new UserEqualityComparer())
                           .Select(g => new PerformerWithTasksDTO
                           {
                               Id = g.Key.Id,
                               TeamId = g.Key.TeamId,
                               FirstName = g.Key.FirstName,
                               LastName = g.Key.LastName,
                               Email = g.Key.Email,
                               RegisteredAt = g.Key.RegisteredAt,
                               Birthday = g.Key.Birthday,
                               Tasks = g.OrderByDescending(t => t.Name.Length).ToList()
                           })
                           .OrderBy(performer => performer.FirstName)
                           .ToList();
        }

        public async Task<UserDetailDTO> GetUserDetail(int userId)
        {
            IEnumerable<ProjectDTO> projects = await CreateStructure();
            return projects.Where(p => p.AuthorId == userId)
                           .GroupBy(p => p.Author, new UserEqualityComparer())
                           .Select(g => new
                           {
                               User = g.Key,
                               LastProject = g.OrderByDescending(p => p.CreatedAt).First(),
                               UserTasks = projects.SelectMany(p => p.Tasks).Where(t => t.PerformerId == g.Key.Id)
                           })
                           .Select(g => new UserDetailDTO
                           {
                               User = g.User,
                               LastProject = g.LastProject,
                               ProjectTasksCount = g.LastProject.Tasks.Count(),
                               UserUnfinishedTasksCount = g.UserTasks.Count(t => !t.FinishedAt.HasValue),
                               LongestTask = g.UserTasks.OrderByDescending(t => t.FinishedAt.HasValue ? (t.FinishedAt - t.CreatedAt) : (DateTime.Now - t.CreatedAt)).FirstOrDefault()
                           })
                           .FirstOrDefault();
        }

        public async Task<IEnumerable<ProjectDetailDTO>> GetProjectsDetail()
        {
            IEnumerable<ProjectDTO> projects = await CreateStructure();
            const int MinDescriptionLength = 20;
            const int MaxProjectTasks = 3;
            return projects.Select(p => new ProjectDetailDTO
                            {
                                Project = p,
                                LongestDescriptionTask = p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                                ShortestNameTask = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                                TeamMembersCount = p.Description.Length > MinDescriptionLength || p.Tasks.Count() < MaxProjectTasks ? p.Team.Members.Count() : new int?()
                            })
                           .ToList();
        }
    }
}
