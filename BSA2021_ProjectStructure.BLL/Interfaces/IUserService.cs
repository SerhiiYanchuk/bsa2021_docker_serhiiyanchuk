﻿using BSA2021_ProjectStructure.Common.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.BLL.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDTO>>  GetAll();
        Task<UserDTO> FindById(int id);
        Task<UserDTO> Insert(NewUserDTO item);
        Task Update(UserDTO item);
        Task Delete(int id);
        Task Delete(UserDTO item);
        Task<bool> CheckAvailability(int id);
        Task<IEnumerable<TaskDTO>> FindUserUnfinishedTasks(int id);
    }
}
