﻿using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.Common.DTO.QueryResultDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.BLL.Interfaces
{
    public interface ILinqSelectionService
    {
        Task<IDictionary<ProjectDTO, int>> GetUserProjectsWithQuantityTask(int performerId);
        Task<IEnumerable<TaskDTO>> GetUserTasks(int performerId);
        Task<IEnumerable<FinishedTaskDTO>> GetFinishedTaskInCurrentYear(int performerId);
        Task<IEnumerable<TeamShortInfoDTO>> GetTeamShortInfo();
        Task<IEnumerable<PerformerWithTasksDTO>> GetUsersWithTasks();
        Task<UserDetailDTO> GetUserDetail(int userId);
        Task<IEnumerable<ProjectDetailDTO>> GetProjectsDetail();
    }
}
