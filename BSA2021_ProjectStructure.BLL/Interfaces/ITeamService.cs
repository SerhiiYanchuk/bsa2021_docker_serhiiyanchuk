﻿using BSA2021_ProjectStructure.Common.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.BLL.Interfaces
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamDTO>> GetAll();
        Task<TeamDTO> FindById(int id);
        Task<TeamDTO> Insert(NewTeamDTO item);
        Task Update(TeamDTO item);
        Task Delete(int id);
        Task Delete(TeamDTO item);
        Task<bool> CheckAvailability(int id);
    }
}
