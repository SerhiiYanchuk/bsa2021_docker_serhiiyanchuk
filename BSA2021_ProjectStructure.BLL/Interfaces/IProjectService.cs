﻿using BSA2021_ProjectStructure.Common.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.BLL.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectDTO>> GetAll();
        Task<ProjectDTO> FindById(int id);
        Task<ProjectDTO> Insert(NewProjectDTO item);
        Task Update(ProjectDTO item);
        Task Delete(int id);
        Task Delete(ProjectDTO item);
        Task<bool> CheckAvailability(int id);
    }
}
