FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY *.sln ./
COPY BSA2021_ProjectStructure.WebAPI/BSA2021_ProjectStructure.WebAPI.csproj BSA2021_ProjectStructure.WebAPI/
COPY BSA2021_ProjectStructure.Common/BSA2021_ProjectStructure.Common.csproj BSA2021_ProjectStructure.Common/
COPY BSA2021_ProjectStructure.DAL/BSA2021_ProjectStructure.DAL.csproj BSA2021_ProjectStructure.DAL/
COPY BSA2021_ProjectStructure.BLL/BSA2021_ProjectStructure.BLL.csproj BSA2021_ProjectStructure.BLL/
RUN dotnet restore
COPY . .
WORKDIR /src/BSA2021_ProjectStructure.WebAPI
RUN dotnet build -c Release -o /app/build

FROM build AS publish
RUN dotnet publish -c Release -o /app/publish

FROM base AS final
WORKDIR /app
EXPOSE 80
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "BSA2021_ProjectStructure.WebAPI.dll"]