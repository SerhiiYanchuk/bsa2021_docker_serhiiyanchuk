﻿using BSA2021_ProjectStructure.Common.DTO;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace BSA2021_ProjectStructure.Common.EqualityComparer
{
    public class ProjectEqualityComparer : IEqualityComparer<ProjectDTO>
    {
        public bool Equals(ProjectDTO x, ProjectDTO y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode([DisallowNull] ProjectDTO obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
