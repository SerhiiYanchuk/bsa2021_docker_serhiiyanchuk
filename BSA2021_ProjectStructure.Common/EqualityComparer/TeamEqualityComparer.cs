﻿using BSA2021_ProjectStructure.Common.DTO;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace BSA2021_ProjectStructure.Common.EqualityComparer
{
    public class TeamEqualityComparer : IEqualityComparer<TeamDTO>
    {
        public bool Equals(TeamDTO x, TeamDTO y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode([DisallowNull] TeamDTO obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
