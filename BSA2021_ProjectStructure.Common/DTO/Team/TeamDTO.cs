﻿using System;
using System.Collections.Generic;


namespace BSA2021_ProjectStructure.Common.DTO
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<UserDTO> Members { get; set; } = new List<UserDTO>();
    }
}
