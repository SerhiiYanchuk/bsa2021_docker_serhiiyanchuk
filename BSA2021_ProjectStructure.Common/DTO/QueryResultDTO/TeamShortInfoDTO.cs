﻿using System.Collections.Generic;

namespace BSA2021_ProjectStructure.Common.DTO.QueryResultDTO
{
    public class TeamShortInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDTO> Members { get; set; } = new List<UserDTO>();
    }
}
