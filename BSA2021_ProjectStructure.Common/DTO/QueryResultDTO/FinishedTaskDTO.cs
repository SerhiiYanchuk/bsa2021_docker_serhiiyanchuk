﻿

namespace BSA2021_ProjectStructure.Common.DTO.QueryResultDTO
{
    public class FinishedTaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
