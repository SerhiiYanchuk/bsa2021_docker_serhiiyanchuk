﻿using System;
using System.Collections.Generic;


namespace BSA2021_ProjectStructure.Common.DTO
{
    public class NewProjectDTO
    {
        public int AuthorId { get; set; }      
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
