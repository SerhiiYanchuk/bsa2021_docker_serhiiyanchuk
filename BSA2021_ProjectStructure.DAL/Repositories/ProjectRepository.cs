﻿using BSA2021_ProjectStructure.DAL.Context;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using Task = System.Threading.Tasks.Task;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public ProjectRepository(ProjectDbContext context) : base(context)
        {
        }

        // you can implement specific functionality and override the base implementation of CRUD
        public async Task LoadTasks(Project project)
        {
            await _context.Entry(project).Collection(p => p.Tasks).LoadAsync();
        }
        public async Task LoadAuthor(Project project)
        {
            await _context.Entry(project).Reference(p => p.Author).LoadAsync();
        }
        public async Task LoadTeam(Project project)
        {
            await _context.Entry(project).Reference(p => p.Team).LoadAsync();
        }
    }
}
