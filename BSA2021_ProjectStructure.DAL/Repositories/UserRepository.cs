﻿using BSA2021_ProjectStructure.DAL.Context;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using Task = System.Threading.Tasks.Task;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ProjectDbContext context) : base(context)
        {
        }

        // you can implement specific functionality and override the base implementation of CRUD
        public async Task LoadProjects(User user)
        {
            await _context.Entry(user).Collection(p => p.Projects).LoadAsync();
        }
        public async Task LoadTasks(User user)
        {
            await _context.Entry(user).Collection(p => p.Tasks).LoadAsync();
        }
    }
}
