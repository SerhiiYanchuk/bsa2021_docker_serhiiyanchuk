﻿using BSA2021_ProjectStructure.DAL.Context;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public class TaskRepository : Repository<Task>, ITaskRepository
    {
        public TaskRepository(ProjectDbContext context) : base(context)
        {
        }

        // you can implement specific functionality and override the base implementation of CRUD
        public async System.Threading.Tasks.Task LoadProject(Task task)
        {
            await _context.Entry(task).Reference(p => p.Project).LoadAsync();
        }
        public async System.Threading.Tasks.Task LoadPerformer(Task task)
        {
            await _context.Entry(task).Reference(p => p.Performer).LoadAsync();
        }
    }
}
