﻿

namespace BSA2021_ProjectStructure.DAL.Entities
{
    public enum TaskState: byte
    {
        Undefined = 0,
        Unfinished,
        Finished,
        Canceled
    }
}
