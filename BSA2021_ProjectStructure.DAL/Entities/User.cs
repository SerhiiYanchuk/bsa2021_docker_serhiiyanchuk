﻿using BSA2021_ProjectStructure.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BSA2021_ProjectStructure.DAL.Entities
{
    public class User : Entity
    {
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public List<Project> Projects { get; set; } = new List<Project>();
        public List<Task> Tasks { get; set; } = new List<Task>();
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime Birthday { get; set; }
    }
}


