﻿using BSA2021_ProjectStructure.DAL.Entities;

namespace BSA2021_ProjectStructure.DAL.Interfaces
{
    public interface IProjectRepository : IRepository<Project>
    {
        // you can add specific functionality for Project
        System.Threading.Tasks.Task LoadTasks(Project project);
        System.Threading.Tasks.Task LoadAuthor(Project project);
        System.Threading.Tasks.Task LoadTeam(Project project);
    }
}
