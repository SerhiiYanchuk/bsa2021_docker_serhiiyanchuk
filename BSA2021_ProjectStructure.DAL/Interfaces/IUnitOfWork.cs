﻿using System;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IProjectRepository Projects { get; }
        ITaskRepository Tasks { get; }
        ITeamRepository Teams { get; }
        IUserRepository Users { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
