﻿using BSA2021_ProjectStructure.DAL.Entities;

namespace BSA2021_ProjectStructure.DAL.Interfaces
{
    public interface ITeamRepository : IRepository<Team>
    {
        // you can add specific functionality for Team
        System.Threading.Tasks.Task LoadMembers(Team team);
        System.Threading.Tasks.Task LoadProjects(Team team);
    }
}
