﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BSA2021_ProjectStructure.DAL.Migrations
{
    public partial class ColumnNameAltered : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BirthDay",
                table: "Users",
                newName: "Birthday");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Birthday",
                table: "Users",
                newName: "BirthDay");
        }
    }
}
