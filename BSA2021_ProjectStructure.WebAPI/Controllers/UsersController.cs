﻿using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.Common.DTO.QueryResultDTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA2021_UserStructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILinqSelectionService _linqSelectionService;

        public UsersController(IUserService userService, ILinqSelectionService linqSelectionService)
        {
            _userService = userService;
            _linqSelectionService = linqSelectionService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUsers()
        {
            return Ok(await _userService.GetAll());
        }
        [HttpGet("tasks")]
        public async Task<ActionResult<IEnumerable<PerformerWithTasksDTO>>> GetUsersWithTasks()
        {
            return Ok(await _linqSelectionService.GetUsersWithTasks());
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetUser(int id)
        {
            var user = await _userService.FindById(id);
            if (user is null)
                return NotFound("ID doesn't exist");
            return Ok(user);
        }
        [HttpGet("{id}/projects")]
        public async Task<ActionResult<IDictionary<ProjectDTO, int>>> GetUserProjectsWithQuantityTask(int id)
        {
            if (await _userService.CheckAvailability(id) == false)
                return NotFound("ID doesn't exist");
            var selection = await _linqSelectionService.GetUserProjectsWithQuantityTask(id);
            return Ok(selection.ToArray());
        }
        [HttpGet("{id}/tasks")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUserTasks(int id)
        {
            if (await _userService.CheckAvailability(id) == false)
                return NotFound("ID doesn't exist");
            return Ok(await _linqSelectionService.GetUserTasks(id));
        }
        [HttpGet("{id}/tasks/finished")]
        public async Task<ActionResult<IEnumerable<FinishedTaskDTO>>> GetFinishedTaskInCurrentYear(int id)
        {
            if (await _userService.CheckAvailability(id) == false)
                return NotFound("ID doesn't exist");
            return Ok(await _linqSelectionService.GetFinishedTaskInCurrentYear(id));
        }
        [HttpGet("{id}/tasks/unfinished")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUserUnfinishedTasks(int id)
        {
            if (await _userService.CheckAvailability(id) == false)
                return NotFound("ID doesn't exist");
            return Ok(await _userService.FindUserUnfinishedTasks(id));
        }
        [HttpGet("{id}/detail")]
        public async Task<ActionResult<UserDetailDTO>> GetUserDetail(int id)
        {
            if (await _userService.CheckAvailability(id) == false)
                return NotFound("ID doesn't exist");
            return Ok(await _linqSelectionService.GetUserDetail(id));
        }
        [HttpPost]
        public async Task<ActionResult<UserDTO>> PostUser([FromBody] NewUserDTO userDTO)
        {
            var createdUser = await _userService.Insert(userDTO);
            return Created("/api/users/" + createdUser.Id, createdUser);
        }

        [HttpPut]
        public async Task<IActionResult> PutUser([FromBody] UserDTO userDTO)
        {
            if (await _userService.CheckAvailability(userDTO.Id) == false)
                return NotFound("ID doesn't exist");

            await _userService.Update(userDTO);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            if (await _userService.CheckAvailability(id) == false)
                return NotFound("ID doesn't exist");

            await _userService.Delete(id);
            return NoContent();
        }
    }
}
