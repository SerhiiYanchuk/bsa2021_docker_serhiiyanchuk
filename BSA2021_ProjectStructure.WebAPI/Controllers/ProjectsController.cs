﻿using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.Common.DTO.QueryResultDTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> GetProjects()
        {
            return Ok(await _projectService.GetAll());
        }

        [HttpGet("detail")]
        public async Task<ActionResult<IEnumerable<ProjectDetailDTO>>> GetProjectsDetail([FromServices] ILinqSelectionService linqSelectionService)
        {
            return Ok(await linqSelectionService.GetProjectsDetail());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetProject(int id)
        {
            var project = await _projectService.FindById(id);
            if (project is null)
                return NotFound("ID doesn't exist");
            return Ok(project);
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> PostProject([FromBody] NewProjectDTO projectDTO)
        {
            var createdProject = await _projectService.Insert(projectDTO);
            return Created("/api/projects/" + createdProject.Id, createdProject);
        }

        [HttpPut]
        public async Task<IActionResult> PutProject([FromBody] ProjectDTO projectDTO)
        {
            if (await _projectService.CheckAvailability(projectDTO.Id) == false)
                return NotFound("ID doesn't exist");

            await _projectService.Update(projectDTO);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProject(int id)
        {
            if (await _projectService.CheckAvailability(id) == false)
                return NotFound("ID doesn't exist");

            await _projectService.Delete(id);
            return NoContent();
        }
    }
}
