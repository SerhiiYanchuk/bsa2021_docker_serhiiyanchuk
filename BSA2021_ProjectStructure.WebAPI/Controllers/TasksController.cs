﻿using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA2021_TaskStructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasks()
        {
            return Ok(await _taskService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> GetTask(int id)
        {
            var task = await _taskService.FindById(id);
            if (task is null)
                return NotFound("ID doesn't exist");
            return Ok(task);
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> PostTask([FromBody] NewTaskDTO taskDTO)
        {
            var createdTask = await _taskService.Insert(taskDTO);
            return Created("/api/tasks/" + createdTask.Id, createdTask);
        }

        [HttpPut]
        public async Task<IActionResult> PutTask([FromBody] TaskDTO taskDTO)
        {
            if (await _taskService.CheckAvailability(taskDTO.Id) == false)
                return NotFound("ID doesn't exist");

            await _taskService.Update(taskDTO);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTask(int id)
        {
            if (await _taskService.CheckAvailability(id) == false)
                return NotFound("ID doesn't exist");

            await _taskService.Delete(id);
            return NoContent();
        }
    }
}
